# Copyright (C) 2023 The hunter2 Contributors.
#
# This file is part of hunter2 API.
#
# hunter2 API is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# hunter2 API is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with hunter2 API. If not, see <https://www.gnu.org/licenses/>.

import sys

from jsonmodels import models, fields, validators
from jsonmodels.errors import ValidationError
import inflection


class Message(models.Base):
    @staticmethod
    def from_struct(struct, validate=True):
        try:
            class_name = inflection.camelize(inflection.underscore(struct['type']))
            message = getattr(sys.modules[__name__], class_name)()
            message.populate(**struct['content'])
        except (AttributeError, KeyError):
            raise ValidationError()
        if validate:
            message.validate()
        return message

    def to_struct(self):
        return {
            'type': inflection.dasherize(inflection.underscore(type(self).__name__)),
            'content': super().to_struct(),
        }


class Error(Message):
    code = fields.StringField(required=True, validators=validators.Enum(
        "IMPOLITE_MESSAGE",
        "MISSING_TYPE",
        "INVALID_TYPE",
        "MISSING_TOKEN",
        "MALFORMED_TOKEN",
        "INVALID_TOKEN",
        "INVALID_MESSAGE",
        "UNAUTHORIZED_TOKEN",
        "INTERNAL_ERROR",
    ))
    message = fields.StringField(required=True)


class EpisodeFinishersPlz(Message):
    api_token = fields.StringField(required=True)
    episode_ids = fields.ListField([int])


class EpisodeFinished(Message):
    episode_id = fields.IntField(required=True)
    team_id = fields.IntField(required=True)


class PuzzleFinishersPlz(Message):
    api_token = fields.StringField(required=True)
    puzzle_ids = fields.ListField([int])


class PuzzleFinished(Message):
    puzzle_id = fields.IntField(required=True)
    team_id = fields.IntField(required=True)
