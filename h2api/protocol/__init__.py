# Copyright (C) 2023 The hunter2 Contributors.
#
# This file is part of hunter2 API.
#
# hunter2 API is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# hunter2 API is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with hunter2 API. If not, see <https://www.gnu.org/licenses/>.
